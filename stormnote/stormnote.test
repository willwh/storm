<?php
/**
 * @file
 * Test definitions for the Storm note module
 */
class StormnoteTestCase extends DrupalWebTestCase {

  public static function getInfo() {
    return array(
      'name' => t('Storm Note Functionality'),
      'description' => t('Test the functionality of the Storm Note module'),
      'group' => 'Storm',
    );
  }

  public function setUp() {
    parent::setUp('storm', 'stormorganization', 'stormproject', 'stormtask', 'stormnote');
  }

  public function testStormnoteAccess() {
    $this->drupalGet('storm/notes');
    $this->assertResponse(403, t('Make sure access is denied to Storm Notes list for anonymous user'));

    $basic_user = $this->drupalCreateUser();
    $this->drupalLogin($basic_user);
    $this->drupalGet('storm/notes');
    $this->assertResponse(403, t('Make sure access is denied to Storm Notes list for basic user'));

    $privileged_user = $this->drupalCreateUser(array('Storm note: access'));
    $this->drupalLogin($privileged_user);
    $this->drupalGet('storm/notes');
    $this->assertText(t('Notes'), t('Make sure the correct page has been displayed by checking that the title is "Notes".'));
  }

  public function testStormnoteCreate() {
    // Create and login user
    $user = $this->drupalCreateUser(array('Storm organization: add', 'Storm organization: view all', 'Storm project: add', 'Storm project: view all', 'Storm task: add', 'Storm task: view all', 'Storm note: add', 'Storm note: view all'));
    $this->drupalLogin($user);

    // Create organization and invoice
    $org = array(
      'title' => $this->randomName(32),
      'body' => $this->randomName(64),
    );
    $prj = array(
      'title' => $this->randomName(32),
      'organization_nid' => '1',
    );
    $task = array(
      'title' => $this->randomName(32),
      'body' => $this->randomName(64),
    );
    $note = array(
      'title' => $this->randomName(32),
      'body' => $this->randomName(64),
    );
    $this->drupalPost('node/add/stormorganization', $org, t('Save'));
    $this->drupalPost('node/add/stormproject', $prj, t('Save'));
    $this->drupalPost('node/add/stormtask', $task, t('Save'));
    $this->drupalPost('node/add/stormnote', $note, t('Save'));

    $this->assertText(t('Note @title has been created.', array('@title' => $note['title'])));;
  }
}
